#Modulo de url de platzigram
from django.urls import include, path
from platzigram import views
from django.contrib import admin



urlpatterns = [
    path('hello_world/', views.hello_world),
    path('', include('usuarios.urls')),
    path('admin/', admin.site.urls),
]
