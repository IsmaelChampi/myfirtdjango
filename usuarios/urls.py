#Modulo de url de platzigram
from django.urls import path
from platzigram import views
from usuarios import views

urlpatterns = [
	path('', views.home),
	path('ajax/', views.ajaxviwew),
	path('usuarios/login/', views.login_view, name='login'),
    path('usuarios/logout/', views.logout_view, name='logout'),
    path('usuarios/update_profile', views.update_profile, name='update_profile'),
    ]