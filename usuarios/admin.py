# -*- coding: utf-8 -*-
from django.contrib import admin
from usuarios.models import Perfil
from django.utils.translation import ugettext as _




@admin.register(Perfil)
class PerfilAdmin(admin.ModelAdmin):
    list_display = ('pk', 'username', 'first_name', 'last_name', 'email')
    search_fields = ['username', 'first_name', 'last_name', 'email']
    ordering = ['first_name', 'last_name']
    list_per_page = 50